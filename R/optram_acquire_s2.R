#' @title Acquire Sentinel 2 Images at a Given Location and Date Range
#' @description Use the `CDSE` package to acquire, preprocess and crop
#'  Sentinel 2 satellite imagery.
#' @param aoi_file, string, full path to polygon spatial file of
#'      boundary of area of interest
#' @param from_date, string, represents start of date range,
#'      formatted as "YYYY-MM-DD"
#' @param to_date, string, end of date range, formatted as "YYYY-MM-DD"
#' @param max_cloud, integer, maximum percent of cloud cover. Default 15.
#' @param output_dir, string, path to save processed imagery.
#' @param veg_index, string, which index to prepare. Default "NDVI".
#'  Can be "NDVI", "SAVI", "MSAVI", etc
#' @param scale_factor, numeric, scale factor for reflectance values.
#'      Default 10000.
#' @param save_creds, logical, whether to save CDSE credentials. Default TRUE.
#' @param clientid, string, user's OAuth client id. Required if `save_creds`
#'      is TRUE.
#' @param secret, string, user's OAuth secret. Required if `save_creds` is TRUE.
#' @param remote, string, from which archive to download imagery
#'    possible values: 'scihub', 'openeo'
#' @param SWIR_band, integer, either 11 or 12, determines which SWIR band to use
#'
#' @return output_path, string, path to downloaded files
#' @export
#' @note
#' This wrapper function calls one of multiple download functions,
#' each accessing a different cloud-based resource.
#' The cloud based resource can be one of "scihub" or "openeo":
#'
#' "scihub",...
#' If "scihub" then:
#' This function utilizes the `CDSE` package.
#' Make sure to install the CDSE and jsonlite packages.
#' Create OAuth account and token:
#' Creating an Account:
#'  1. Navigate to the [Copernicus portal](https://dataspace.copernicus.eu/).
#'  2. Click the "Register" button to access the account creation page.
#'  3. If already registered, enter your username and password,
#'    and click "Login."
#'  4. Once logged in, go to the User dashboard and click "User Settings" to
#'    access the Settings page.
#'
#' Creating OAuth Client:
#'  1. On the Settings page, click the green "Create New" button located on
#'    the right.
#'  2. Enter a suitable "Client Name" and click the green "Create Client"
#'    button.
#'  3. A Client secret is generated.
#' the user must save her secret and clientid somewhere.
#' these credentials will be saved automatically to a standard filesystem
#' location if the user calls check_scihub() with the argument save_creds
#' set to TRUE (recommended).
#' if the user chooses not to save credentials to the standard filesystem
#' location, then she will need to add both clientid and secret to each
#' acquire_scihub() function call.
#'
#' Using Credentials with `aquire_scihub`:
#'  - Now, you can utilize the generated `clientid` and `secret` in
#'    the `aquire_scihub` function.
#'  - If you want to store your credentials on your computer, ensure that when
#'    running `aquire_scihub`, the `save_creds` parameter is set to `TRUE`.
#'  - During the first run of `aquire_scihub`, manually input your `clientid`
#'    and `secret` in the function's signature. Subsequent runs will use the
#'    stored credentials.
#' Subject Area Constraint:
#'  The downloadable images are restricted to a maximum size of 2500 pixels on
#'  each side. This limitation is established due to the final resolution set to
#'  10 meters using JavaScript. Consequently, the subject area available for
#'  download is limited to 25 kilometers in both directions. Please be aware of
#'  this restriction when selecting your desired area for download.

#' Area of Interest (AOI) Specification:
#'  When defining your Area of Interest (AOI), please ensure that it is
#'  represented as a polygonal layer with only one feature. This feature can
#'  either be a single POLYGON or a MULTIPOLYGON, which may consist of
#'  non-contiguous areas, but only one feature is permissible.
#'
#' "openeo",...
#' If "openeo" then:
#' This function utilizes the `openeo` package.
#' Instructions for the login process:
#' First of all, to authenticate your account on the backend of the Copernicus
#' Data Space Ecosystem, it is necessary for you to complete the registration
#' process. Follow these instructions for registration:
#' https://documentation.dataspace.copernicus.eu/Registration.html
#' After you have registered and installed the `openeo` package, you can run the
#' `acquire_openeo` function.
#' During the process of connecting to the server and logging in, you need to
#' follow these steps:
#' A. When the message "Press <enter> to proceed:" appears in the console,
#' press enter.
#' Calling this method opens your system web browser, with which
#' you can authenticate yourself on the back-end authentication system. After
#' that, the website will give you instructions to go back to the R client,
#' where your connection has logged your account in. This means that every
#' call that comes after that via the connection variable is executed by your
#' user account.
#' B. You will be redirected to "https://identity.dataspace.copernicus.eu/".
#' Ensure you have an account and are logged in. You will be required to
#' grant access - press "yes".
#'
#' Two SWIR bands are available in Sentinel-2: 1610 nanometer (nm) and 2190 nm.
#' The parameter `SWIR_bands ` allows to choose which band is used in this model.
#'
#' @examples
#' \dontrun{
#' from_date <- "2018-12-01"
#' to_date <- "2019-04-30"
#' aoi <- system.file("extdata", "lachish.gpkg", package = 'rOPTRAM')
#' s2_file_list <- optram_acquire_s2(aoi,
#'                                  from_date, to_date,
#'                                  remote = "scihub"
#'                                  veg_index = "SAVI")
#' }

optram_acquire_s2 <- function(
      aoi_file,
      from_date, to_date,
      max_cloud = 10,
      output_dir = tempdir(),
      veg_index = "NDVI",
      scale_factor = 10000,
      save_creds = TRUE,
      clientid = NULL,
      secret = NULL,
      remote = c("scihub", "openeo"),
      SWIR_band = c(11, 12)) {
  # Avoid "no visible binding for global variable" NOTE
  scihub <- openeo <- NULL

  # Pre flight checks...
  if (!check_aoi(aoi_file)) return(NULL)
  if (!check_date_string(from_date, to_date)) return(NULL)
  if (!check_swir_band(SWIR_band)) return(NULL)
  remote <- match.arg(remote)

  switch(remote,
         scihub = acquire_scihub(aoi_file = aoi_file,
                                 from_date = from_date, to_date = to_date,
                                 max_cloud = max_cloud,
                                 output_dir = output_dir,
                                 veg_index = veg_index,
                                 save_creds = save_creds,
                                 clientid = clientid,
                                 secret = secret,
                                 SWIR_band = SWIR_band),
         openeo = acquire_openeo(aoi_file = aoi_file,
                                 from_date = from_date, to_date = to_date,
                                 max_cloud = max_cloud,
                                 output_dir = output_dir,
                                 veg_index = veg_index,
                                 scale_factor = scale_factor,
                                 SWIR_band = SWIR_band)
         )
}
