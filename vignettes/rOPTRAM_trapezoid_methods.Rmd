---
title: "rOPTRAM: Three Trapezoid Fitting Methods"
output: rmarkdown::html_document
vignette: >
  %\VignetteIndexEntry{rOPTRAM-3}
  %\VignetteEngine{knitr::rmarkdown_notangle}
  %\VignetteEncoding{UTF-8}
bibliography: "references.bib"
---



## Introduction  <img align="right" width="100" height="100" src="images/rOPTRAM_logo.jpg">

The algorithm for finding trapezoid wet and dry edges works as follows:

After acquiring a time series of Sentinel-2 images over the study area, both vegetation index (i.e. NDVI or SAVI), and SWIR Transformed Reflectance (STR) rasters are prepared. Pixel values of both indices for all images are collected into a two column table (and plotted as a scatterplot). The vegetation axis (x-axis) is split into a large number of intervals (usually between 50 - 100). The width of each interval is configurable by the user through the `vi_step` parameter in `optram_wetdry_coefficients()`. Then for each interval the top and bottom 5% quantiles of STR values are determined. These point values - VI and STR - are considered to create the fitted wet and dry trapezoid edges.

Three fitting methods are available in {rOPTRAM} to prepare the trapezoid wet and dry edges. For detailed background, see:  @ma_combining_2022. Users can choose between a

 - linear OLS fitted line
 - exponential fit
 - second order polynomial

All fitting methods are derived using the `lm` function in the `R` {stats} package.

The linear OLS fit follows:
$$STR = i + s \cdot VI$$
The exponential fit uses the equation:
$$STR = i \cdot e^{(s \cdot VI)}$$
where STR is the fitted STR value, $i$, and $s$ are the exponential regression intercept, and coefficient and $VI$ is the vegetation index value.

The polynomial fit uses:

$$ STR = \alpha + \beta1 \cdot VI + \beta2 \cdot VI^2$$

The fitting method is chosen by setting the `trapezoid_method` parameter in the `optram_wetdry_coefficients()` function.

## Examples



```r
remotes::install_gitlab("rsl-bidr/roptram")
library(rOPTRAM)
if (!require("CDSE")) install.packages("CDSE", dependencies = TRUE)
if (!require("jsonlite")) install.packages("jsonlite", dependencies = TRUE)
```

#### Prepare data.frame of pixel values


```r
from_date <- "2022-05-01"
to_date <- "2023-04-30"
output_dir <- tempdir()
aoi_file <- system.file("extdata", "lachish.gpkg", package = "rOPTRAM")
veg_index <- "NDVI"
s2_file_list <- optram_acquire_s2(aoi_file,
                            from_date, to_date,
                            output_dir = output_dir,
                            veg_index =veg_index,
                            remote = "scihub",
                            SWIR_band = 11)
STR_list <- list.files(file.path(output_dir, "STR"),
                      pattern = ".tif$", full.names = TRUE)
VI_list <- list.files(file.path(output_dir, "NDVI"),
                      pattern = ".tif$", full.names = TRUE)
full_df <- optram_ndvi_str(STR_list, VI_list,
                           output_dir = output_dir,
                           rm.low.vi = TRUE, rm.hi.str = TRUE)
```

#### Show Linear trapezoid plot


```r
meth <- "linear"
rmse <- optram_wetdry_coefficients(full_df,
                                   aoi_file = aoi_file,
                                   output_dir = output_dir,
                                   trapezoid_method = meth,
                                   save_plot = TRUE)
edges_df <- read.csv(file.path(output_dir, "trapezoid_edges_lin.csv"))
plot_vi_str_cloud(full_df, ttl,
                  edges_df = edges_df,
                  trapezoid_method = meth,
                  output_dir = output_dir,
                  edge_points = TRUE)
#> Error in paste("Trapezoid Plot - ", aoi_name): object 'ttl' not found
knitr::include_graphics("vignettes/images/trapezoid_lachish_linear.png")
#> Error in knitr::include_graphics("vignettes/images/trapezoid_lachish_linear.png"): Cannot find the file(s): "vignettes/images/trapezoid_lachish_linear.png"
```

#### Show Exponential fitted trapezoid plot


```r
meth <- "exponential"
coeffs <- optram_wetdry_coefficients(full_df,
                                     aoi_file = aoi_file,
                                     output_dir = output_dir,
                                     trapezoid_method = meth,
                                     save_plot = TRUE)
edges_df <- read.csv(file.path(output_dir, "trapezoid_edges_exp.csv"))
plot_vi_str_cloud(full_df, ttl,
                  output_dir = output_dir,
                  edges_df = edges_df,
                  trapezoid_method = meth,
                  edge_points = TRUE)
#> Error in paste("Trapezoid Plot - ", aoi_name): object 'ttl' not found
knitr::include_graphics("vignettes/images/trapezoid_lachish_exponential.png")
#> Error in knitr::include_graphics("vignettes/images/trapezoid_lachish_exponential.png"): Cannot find the file(s): "vignettes/images/trapezoid_lachish_exponential.png"
```

#### Show Polynomial fitted trapezoid plot


```r
meth <- "polynomial"
coeffs <- optram_wetdry_coefficients(full_df,
                                     aoi_file = aoi_file,
                                     output_dir = output_dir,
                                     trapezoid_method = meth,
                                     save_plot = TRUE)
edges_df <- read.csv(file.path(output_dir, "trapezoid_edges_poly.csv"))
plot_vi_str_cloud(full_df, ttl,
                  output_dir = output_dir,
                  edges_df = edges_df,
                  trapezoid_method = meth,
                  edge_points = TRUE)
#> Error in paste("Trapezoid Plot - ", aoi_name): object 'ttl' not found
knitr::include_graphics("vignettes/images/trapezoid_lachish_polynomial.png")
#> Error in knitr::include_graphics("vignettes/images/trapezoid_lachish_polynomial.png"): Cannot find the file(s): "vignettes/images/trapezoid_lachish_polynomial.png"
```
